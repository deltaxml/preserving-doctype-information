# Preserving Doctype Information
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

How to include doctype information in the result file that is determined by the input file doctype rather than hard-coded into the pipeline.

This document describes how to run the sample. For concept details see: [Preserving Doctype Information](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/preserving-doctype-information)

## Using Java API
If you have Ant installed, use the build script provided to run this sample. Simply type the following command to run the pipeline and produce the output files `result-BdA.xhtml` and `result-B.xhtml`.

	ant run

## Using DXP configuration file
Run the sample from the command line by issuing the following commands from the sample directory (ensuring that you use the correct directory separator for your operating system). Replace x.y.z with the major.minor.patch version number of your release e.g. `deltaxml-10.0.0.jar`

	java -jar ../../deltaxml-x.y.z.jar compare doctypes input1.xhtml input2.xhtml result-BdA.xhtml doctypeMode=BdA
	java -jar ../../deltaxml-x.y.z.jar compare doctypes input1.xhtml input2.xhtml result-B.xhtml doctypeMode=B
	
To clean up the sample directory, run the following command in Ant.
	
	ant clean

