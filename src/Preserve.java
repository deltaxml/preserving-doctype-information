// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.deltaxml.core.ComparatorInstantiationException;
import com.deltaxml.core.FeatureNotRecognizedException;
import com.deltaxml.core.FeatureSettingNotSupportedException;
import com.deltaxml.core.ParserInstantiationException;
import com.deltaxml.core.PipelinedComparatorError;
import com.deltaxml.cores9api.ComparisonException;
import com.deltaxml.cores9api.FilterProcessingException;
import com.deltaxml.cores9api.LicenseException;
import com.deltaxml.cores9api.PipelineLoadingException;
import com.deltaxml.cores9api.PipelineSerializationException;
import com.deltaxml.cores9api.PipelinedComparatorS9;
import com.deltaxml.cores9api.config.LexicalPreservationConfig;
import com.deltaxml.cores9api.config.PreservationProcessingMode;

/**
 * A simple class for preserving doctype (and internal subset) data, when using a 'Core S9API' pipelined comparator.
 */
public class Preserve {

  /**
   * Compare two input files and return a result file.
   */
  public static void main(String[] args) {
    if (args.length != 4) {
      System.out.println("java -cp <deltaxml.jar>:class Preserve <in1> <in2> <out> <mode>");
      System.exit(1);
    }

    try {
      // Use the base pre-defined preservation mode, which essentially provides a
      // neutral starting point, where only text, attribute and element nodes are
      // being preserved.
      LexicalPreservationConfig lpc= new LexicalPreservationConfig("base");

      // Enable a doctype (and its internal subset) to be converted into elements
      // for the purposes of comparison.
      lpc.setPreserveDoctype(true);

      // Use the 'B' version of the doctype (and its internal subset), otherwise
      // leave all other preserved items alone.
      lpc.setDefaultProcessingMode(PreservationProcessingMode.CHANGE);
      lpc.setDoctypeProcessingMode(PreservationProcessingMode.getProcessingMode(args[3]));

      // Construct a pipelined comparator, then configure its lexical preservation
      // as defined above.
      PipelinedComparatorS9 pc= new PipelinedComparatorS9();
      pc.setLexicalPreservationConfig(lpc);

      // w3.org gives 503 errors for DTD requests, we dont actually need it for this sample
      pc.setParserFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

      // Run the comparison
      pc.compare(new File(args[0]), new File(args[1]), new File(args[2]));

    } catch (ParserInstantiationException e) {
      e.printStackTrace();
    } catch (ComparatorInstantiationException e) {
      e.printStackTrace();
    } catch (ComparisonException e) {
      e.printStackTrace();
    } catch (FilterProcessingException e) {
      e.printStackTrace();
    } catch (PipelineLoadingException e) {
      e.printStackTrace();
    } catch (PipelineSerializationException e) {
      e.printStackTrace();
    } catch (LicenseException e) {
      e.printStackTrace();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (PipelinedComparatorError e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (FeatureSettingNotSupportedException e) {
      e.printStackTrace();
    } catch (FeatureNotRecognizedException e) {
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    }
  }
}
